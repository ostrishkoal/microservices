package ostrishkoal.common.model;

import java.math.BigDecimal;

public class ProductPrice {
    private String productId;
    private BigDecimal price;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "ProductPrice{" + "productId='" + productId + '\'' + ", price=" + price + '}';
    }
}
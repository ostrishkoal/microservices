package ostrishkoal.common.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;
import ostrishkoal.common.model.*;

@FeignClient
public interface PriceService {
    @RequestMapping(method = RequestMethod.GET, value = "/avg/price", consumes = "application/json")
    Status checkAvgProduct(ProductPrice productPrice);

    @RequestMapping(method = RequestMethod.GET, value = "/create/price", consumes = "application/json")
    Status createProduct(ProductPrice productPrice);
}
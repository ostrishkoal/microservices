package ostrishkoal.common.client;

import feign.codec.*;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.*;

import static feign.Feign.builder;


public class FeignClient<T> {
    private T service;

    public FeignClient(Class<T> clazz, String url) {
        Encoder encoder = new SpringEncoder(HttpMessageConverters::new);
        Decoder decoder = new ResponseEntityDecoder(new SpringDecoder(HttpMessageConverters::new));
        service = builder().encoder(encoder).decoder(decoder).contract(new SpringMvcContract()).target(clazz, url);
    }

    public T getService() {
        return service;
    }
}
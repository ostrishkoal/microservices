package ostrishkoal.common.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import ostrishkoal.common.model.*;

@FeignClient
public interface ApiService {
    @RequestMapping(value = "/check/product", consumes = "application/json")
    Status checkProduct(Product product);
}
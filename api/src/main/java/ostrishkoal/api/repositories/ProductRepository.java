package ostrishkoal.api.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductRepository extends MongoRepository<ProductDoc, String> {
    ProductDoc findByName(String name);
}
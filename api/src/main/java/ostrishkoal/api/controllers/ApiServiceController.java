package ostrishkoal.api.controllers;

import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.*;
import ostrishkoal.api.repositories.*;
import ostrishkoal.common.client.*;
import ostrishkoal.common.model.*;

import javax.annotation.PostConstruct;

@RestController
@Configuration
@ConfigurationProperties
public class ApiServiceController implements ApiService {
    @Value("${priceservice.url}")
    private String url;
    @Autowired
    private ProductRepository repository;
    private PriceService priceService;

    @PostConstruct
    public void init() {
        FeignClient<PriceService> client = new FeignClient<>(PriceService.class, url);
        priceService = client.getService();
    }

    @Override
    @RequestMapping(value = "/check/product", consumes = "application/json")
    public Status checkProduct(@RequestBody Product product) {
        ProductDoc productDB = repository.findByName(product.getName());
        ProductPrice productPrice = new ProductPrice();
        productPrice.setPrice(product.getPrice());
        if (productDB != null) {
            productPrice.setProductId(productDB.getId());
            return priceService.checkAvgProduct(productPrice);
        } else {
            ProductDoc prodDoc = new ProductDoc();
            prodDoc.setName(product.getName());
            repository.save(prodDoc);
            productPrice.setProductId(prodDoc.getId());
            return priceService.createProduct(productPrice);
        }
    }
}
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ostrishkoal.api.repositories.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@EnableMongoRepositories(basePackageClasses = ProductRepository.class)
@ContextConfiguration(classes = {MongoTestConfig.class})
@EnableAutoConfiguration(exclude = {EmbeddedMongoAutoConfiguration.class})
public class ApiServiceTest {
    @Autowired
    private ProductRepository repository;

    @Test
    public void test() {
        ProductDoc prodDoc = new ProductDoc();
        prodDoc.setName("w");
        //repository.save(prodDoc);
        System.out.println(repository.findAll());
    }
}
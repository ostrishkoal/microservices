package ostrishkoal.priceservice.repositories;

import ostrishkoal.common.model.ProductPrice;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "product_price")
public class ProductPriceEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String productId;
    private BigDecimal price;

    public ProductPriceEntity() {
    }

    public ProductPriceEntity(ProductPrice pp) {
        productId = pp.getProductId();
        price = pp.getPrice();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "ProductPriceEntity{" + "id=" + id + ", productId='" + productId + '\'' + ", price=" + price + '}';
    }
}
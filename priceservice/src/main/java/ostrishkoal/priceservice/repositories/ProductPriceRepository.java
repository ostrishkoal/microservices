package ostrishkoal.priceservice.repositories;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import javax.persistence.criteria.*;
import java.math.BigDecimal;
import java.util.List;

@Repository
public class ProductPriceRepository {
    @PersistenceContext
    private EntityManager em;

    public BigDecimal getAvgPrice(ProductPriceEntity ppEntity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<BigDecimal> q = cb.createQuery(BigDecimal.class);
        Root<ProductPriceEntity> c = q.from(ProductPriceEntity.class);
        q.multiselect(cb.avg(c.get("price")));
        q.where(cb.equal(c.get("productId"), ppEntity.getProductId()));
        q.groupBy(c.get("productId"));
        return em.createQuery(q).getSingleResult();
    }

    @Transactional
    public void create(ProductPriceEntity ppEntity) {
        em.persist(ppEntity);
    }

    public List<ProductPriceEntity> list() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<ProductPriceEntity> cq = cb.createQuery(ProductPriceEntity.class);
        Root<ProductPriceEntity> rootEntry = cq.from(ProductPriceEntity.class);
        CriteriaQuery<ProductPriceEntity> all = cq.select(rootEntry);
        TypedQuery<ProductPriceEntity> allQuery = em.createQuery(all);
        return allQuery.getResultList();
    }
}
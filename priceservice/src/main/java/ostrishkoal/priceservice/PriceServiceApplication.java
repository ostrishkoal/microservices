package ostrishkoal.priceservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ostrishkoal.priceservice.config.JpaConfig;
import ostrishkoal.priceservice.repositories.ProductPriceEntity;

@EnableFeignClients
@EnableTransactionManagement
@EntityScan(basePackageClasses = ProductPriceEntity.class)
@ContextConfiguration(classes = {JpaConfig.class})
@SpringBootApplication
public class PriceServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(PriceServiceApplication.class, args);
    }
}
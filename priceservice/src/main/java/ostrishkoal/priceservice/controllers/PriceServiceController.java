package ostrishkoal.priceservice.controllers;

import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.*;
import ostrishkoal.common.client.PriceService;
import ostrishkoal.common.model.*;
import ostrishkoal.priceservice.repositories.*;

import java.math.BigDecimal;

import static ostrishkoal.common.model.Status.*;

@RestController
@Configuration
@ConfigurationProperties
public class PriceServiceController implements PriceService {
    @Value("${priceservice.rangePrice.leftCoeffToAvg}")
    private BigDecimal leftCoeffToAvg;
    @Value("${priceservice.rangePrice.rightCoeffToAvg}")
    private BigDecimal rightCoeffToAvg;
    @Autowired
    private ProductPriceRepository repository;

    @Override
    @RequestMapping(value = "/avg/price", consumes = "application/json")
    public Status checkAvgProduct(@RequestBody ProductPrice productPrice) {
        ProductPriceEntity ppEntity = new ProductPriceEntity(productPrice);
        BigDecimal avg = repository.getAvgPrice(ppEntity);
        BigDecimal leftBorder = leftCoeffToAvg.multiply(avg);
        BigDecimal rightBorder = rightCoeffToAvg.multiply(avg);
        if (leftBorder.compareTo(productPrice.getPrice()) < 0 && rightBorder.compareTo(productPrice.getPrice()) > 0) {
            repository.create(ppEntity);
            return OK;
        }
        return ERROR;
    }

    @Override
    @RequestMapping(value = "/create/price", consumes = "application/json")
    public Status createProduct(@RequestBody ProductPrice productPrice) {
        ProductPriceEntity ppEntity = new ProductPriceEntity(productPrice);
        repository.create(ppEntity);
        return OK;
    }
}
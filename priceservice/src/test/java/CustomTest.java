import ostrishkoal.common.client.*;
import ostrishkoal.common.model.*;

import java.math.BigDecimal;

public class CustomTest {
    public static final String API_URL = "http://localhost:49000";

    public static void main(String[] args) {
        Product product = new Product();
        product.setName("zero");
        product.setPrice(new BigDecimal(4));
        FeignClient<ApiService> client = new FeignClient<>(ApiService.class, API_URL);
        ApiService api = client.getService();
        Status status = api.checkProduct(product);
        System.out.println(status);
    }
}
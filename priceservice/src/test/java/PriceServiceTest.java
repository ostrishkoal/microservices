import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ostrishkoal.common.model.ProductPrice;
import ostrishkoal.priceservice.PriceServiceApplication;
import ostrishkoal.priceservice.controllers.PriceServiceController;
import ostrishkoal.priceservice.repositories.*;

import java.math.BigDecimal;

import static org.junit.Assert.*;
import static ostrishkoal.common.model.Status.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {PriceServiceApplication.class, JpaTestConfig.class})
@Sql(value = {"classpath:create.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"classpath:drop.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class PriceServiceTest {
    @Autowired
    private ProductPriceRepository repository;
    @Autowired
    private PriceServiceController controller;

    private ProductPriceEntity ppEntity;
    private ProductPrice pp;

    @Before
    public void init() {
        pp = new ProductPrice();
        pp.setPrice(new BigDecimal(10));
        pp.setProductId("123");
        ppEntity = new ProductPriceEntity(pp);
        repository.create(ppEntity);
    }

    @Test
    public void testRepository() {
        assertEquals(repository.list().size(), 1);
        BigDecimal avg = repository.getAvgPrice(ppEntity);
        assertTrue(avg.compareTo(pp.getPrice()) == 0);
    }

    @Test
    public void checkAvgProduct() {
        assertEquals(OK, controller.checkAvgProduct(pp));
        pp.setPrice(new BigDecimal(2));
        assertEquals(ERROR, controller.checkAvgProduct(pp));
    }

    @Test
    public void createProduct() {
        assertEquals(OK, controller.createProduct(pp));
    }
}
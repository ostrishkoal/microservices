CREATE SCHEMA IF NOT EXISTS test;

CREATE TABLE IF NOT EXISTS test.product_price (
  id         INT          NOT NULL AUTO_INCREMENT,
  product_id VARCHAR(100) NOT NULL,
  price      DECIMAL      NOT NULL,
  PRIMARY KEY (id)
);

COMMIT;